#!/usr/bin/env bash

local_dir="$( cd "$( dirname "$0" )" && pwd )"

${local_dir}/build.sh
kubectl apply -f ${local_dir}/k8s
