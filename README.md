# k8s-express

Intended to be used with <https://gitlab.com/RyanHirsch/k8s-traefik>

- `build.sh` will build and tag the docker image
- `deploy.sh` will build and create the deployment, service, and ingress

The app should then be available at `hello.dev.lan:<TRAEFIK PORT>`. The traefik port can be found by running `kubectl --namespace=kube-system get services`.

[K8s Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
