#!/usr/bin/env bash

local_dir="$( cd "$( dirname "$0" )" && pwd )"

docker build ${local_dir} -t k8s-express
