FROM node:10.9-alpine AS build
ENV NODE_ENV development
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", ".babelrc", "./"]
RUN npm install
COPY ["src", "./src"]
RUN npm run build

FROM node:10.9-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install --production --silent
COPY --from=build ["/usr/src/app/dist/", "./dist"]
EXPOSE 3000
CMD npm start
